package com.lkledu.root.deckbuild;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class ChoseGod extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chose_god);

        final Intent iBuilder = new Intent(ChoseGod.this, Builder.class);

        ImageButton craButton = (ImageButton)findViewById(R.id.cra);
        ImageButton ecaflipButton = (ImageButton)findViewById(R.id.ecaflip);
        ImageButton eniripsaButton = (ImageButton)findViewById(R.id.eniripsa);
        ImageButton enutrofButton = (ImageButton)findViewById(R.id.enutrof);
        ImageButton iopButton = (ImageButton)findViewById(R.id.iop);
        ImageButton sacrierButton = (ImageButton)findViewById(R.id.sacrier);
        ImageButton sadidaButton = (ImageButton)findViewById(R.id.sadida);
        ImageButton sramButton = (ImageButton)findViewById(R.id.sram);
        ImageButton xelorButton = (ImageButton)findViewById(R.id.xelor);

        craButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "cra");
                Log.d("GOD NAME -----", "craClicked");
                startActivity(iBuilder);
            }

        });

        ecaflipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "ecaflip");
                Log.d("GOD NAME -----", "ecaflipClicked");
                startActivity(iBuilder);
            }
        });

        eniripsaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "eniripsa");
                Log.d("GOD NAME -----", "eniripsaClicked");
                startActivity(iBuilder);
            }
        });

        enutrofButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "enutrof");
                Log.d("GOD NAME -----", "enutrofClicked");
                startActivity(iBuilder);
            }
        });

        sacrierButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "sacrier");
                Log.d("GOD NAME -----", "sacrierClicked");
                startActivity(iBuilder);
            }
        });

        sadidaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "sadida");
                Log.d("GOD NAME -----", "sadidaClicked");
                startActivity(iBuilder);
            }
        });

        sramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "sram");
                Log.d("GOD NAME -----", "sramClicked");
                startActivity(iBuilder);
            }
        });

        xelorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iBuilder.putExtra("godName", "xelor");
                Log.d("GOD NAME -----", "xelorClicked");
                startActivity(iBuilder);
            }
        });
    }
}
