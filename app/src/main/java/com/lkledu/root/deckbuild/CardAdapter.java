package com.lkledu.root.deckbuild;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by root on 05/08/17.
 */

public class CardAdapter extends BaseAdapter {
    private Context mContext;
    private String godName;

    //construtor
    public CardAdapter(Context c, String godparam){
        mContext = c;
        this.godName = godparam;
    }

    @Override
    public int getCount() {
        int godCount = 0;
        if(godName.equals("cra")){
            godCount = craIds.length;
        }else if(godName.equals("ecaflip")){
            godCount = ecaflipIds.length;
        }else if(godName.equals("eniripsa")){
            godCount = eniripsaIds.length;
        }else if(godName.equals("enutrof")){
            godCount = enutrofIds.length;
        }else if(godName.equals("iop")){
            godCount = iopIds.length;
        }else if(godName.equals("sacrier")){
            godCount = sacrierIds.length;
        }else if(godName.equals("sadida")){
            godCount = sadidaIds.length;
        }else if(godName.equals("sram")){
            godCount = sramIds.length;
        }else if(godName.equals("xelor")){
            godCount = xelorIds.length;
        }

        return godCount;
    }

    @Override
    public Object getItem(int position) {
        Integer godItem[] = new Integer[position];
        if(godName.equals("cra")){
            for(int i = 0; i <= craIds.length; i++){
                godItem[i] = craIds[i];
            }
        }else if(godName.equals("ecaflip")){
            for(int i = 0; i <= ecaflipIds.length; i++){
                godItem[i] = ecaflipIds[i];
            }
        }else if(godName.equals("eniripsa")){
            for(int i = 0; i <= eniripsaIds.length; i++){
                godItem[i] = eniripsaIds[i];
            }
        }else if(godName.equals("enutrof")){
            for(int i = 0; i <= enutrofIds.length; i++){
                godItem[i] = enutrofIds[i];
            }
        }else if(godName.equals("iop")){
            for(int i = 0; i <= iopIds.length; i++){
                godItem[i] = iopIds[i];
            }
        }else if(godName.equals("sacrier")){
            for(int i = 0; i <= sacrierIds.length; i++){
                godItem[i] = sacrierIds[i];
            }
        }else if(godName.equals("sadida")){
            for(int i = 0; i <= sadidaIds.length; i++){
                godItem[i] = sadidaIds[i];
            }
        }else if(godName.equals("sram")){
            for(int i = 0; i <= sramIds.length; i++){
                godItem[i] = sramIds[i];
            }
        }else if(godName.equals("xelor")){
            for(int i = 0; i <= xelorIds.length; i++){
                godItem[i] = xelorIds[i];
            }
        }

        return godItem[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent) {
        ImageView imageView;

        if(convertview == null){
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(600,600));
            if(godName.equals("cra")){
                imageView.setImageResource(craIds[position]);
            }else if(godName.equals("ecaflip")){
                imageView.setImageResource(ecaflipIds[position]);
            }else if(godName.equals("eniripsa")){
                imageView.setImageResource(eniripsaIds[position]);
            }else if(godName.equals("enutrof")){
                imageView.setImageResource(enutrofIds[position]);
            }else if(godName.equals("iop")){
                imageView.setImageResource(iopIds[position]);
            }else if(godName.equals("sacrier")){
                imageView.setImageResource(sacrierIds[position]);
            }else if(godName.equals("sadida")){
                imageView.setImageResource(sadidaIds[position]);
            }else if(godName.equals("sram")){
                imageView.setImageResource(sramIds[position]);
            }else if(godName.equals("xelor")){
                imageView.setImageResource(xelorIds[position]);
            }
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setPadding(0,0,0,0);
        }else{
            imageView = (ImageView) convertview;
            imageView.setAdjustViewBounds(true);

        }
        if(godName.equals("cra")){
            imageView.setImageResource(craIds[position]);
        }else if(godName.equals("ecaflip")){
            imageView.setImageResource(ecaflipIds[position]);
        }else if(godName.equals("eniripsa")){
            imageView.setImageResource(eniripsaIds[position]);
        }else if(godName.equals("enutrof")){
            imageView.setImageResource(enutrofIds[position]);
        }else if(godName.equals("iop")){
            imageView.setImageResource(iopIds[position]);
        }else if(godName.equals("sacrier")){
            imageView.setImageResource(sacrierIds[position]);
        }else if(godName.equals("sadida")){
            imageView.setImageResource(sadidaIds[position]);
        }else if(godName.equals("sram")){
            imageView.setImageResource(sramIds[position]);
        }else if(godName.equals("xelor")){
            imageView.setImageResource(xelorIds[position]);
        }
        imageView.setAdjustViewBounds(true);

        return imageView;
    }

    private Integer [] craIds= {
            R.drawable.betty_boubz, R.drawable.camouflage, R.drawable.clara_byne, R.drawable.criblage,
            R.drawable.dernier_recours, R.drawable.detournement, R.drawable.eksa_soth, R.drawable.esquive,
            R.drawable.fantome_cra, R.drawable.fleche_chercheuse, R.drawable.fleche_criblante, R.drawable.fleche_de_recul,
            R.drawable.fleche_destructrice, R.drawable.fleche_d_immolation, R.drawable.fleche_explosive,
            R.drawable.fleche_percante, R.drawable.fleche_tempete, R.drawable.guy_yomtella, R.drawable.harcelement,
            R.drawable.jems_blond, R.drawable.lebolas, R.drawable.lucie_fhair, R.drawable.marquage,
            R.drawable.necrome_cra_f, R.drawable.necrome_cra_h, R.drawable.nonne, R.drawable.oeil_de_lynx,
            R.drawable.paladir_cra_f, R.drawable.paladir_cra_h, R.drawable.patty_ceriz, R.drawable.robin_des_landes,
            R.drawable.tireur_d_elite, R.drawable.tir_rapide
    };

    private Integer [] ecaflipIds = {
            R.drawable.arbre_a_chachas, R.drawable.bond_du_felin, R.drawable.bowne_piauch,
            R.drawable.chacha, R.drawable.chasseur, R.drawable.chatar,
            R.drawable.defhi_croquets, R.drawable.de_pipe, R.drawable.de_rebondissant,
            R.drawable.echaenge, R.drawable.karla_blondie, R.drawable.mitaine,
            R.drawable.necrome_eca_f, R.drawable.necrome_eca_h, R.drawable.nomekop_le_crapoteur,
            R.drawable.paladir_eca_f, R.drawable.paladir_eca_h, R.drawable.relance,
            R.drawable.roulette_ecaflip, R.drawable.shafouine, R.drawable.shava_shavien,
            R.drawable.transchamation, R.drawable.trucage, R.drawable.will_skass

    };

    private Integer [] eniripsaIds = {
            R.drawable.abraxane, R.drawable.acide_sandoz, R.drawable.alargix,
            R.drawable.aroma_bome, R.drawable.asprogik_mils, R.drawable.baraklud,
            R.drawable.ben_debouche, R.drawable.dephasage, R.drawable.empathie,
            R.drawable.equite, R.drawable.fiole_de_douleur, R.drawable.fiole_de_frayeur,
            R.drawable.fiole_de_psykoz, R.drawable.fiole_de_torpeur, R.drawable.fiole_infectee,
            R.drawable.incision, R.drawable.lapinos, R.drawable.malox_makugen,
            R.drawable.mot_de_silence, R.drawable.mot_reconstituant, R.drawable.mot_retablissant,
            R.drawable.mot_soignant, R.drawable.necrome_eni_f, R.drawable.necrome_eni_h,
            R.drawable.paladir_eni_f, R.drawable.paladir_eni_h, R.drawable.purge,
            R.drawable.renisurrection, R.drawable.saizan_zen, R.drawable.seduction,
            R.drawable.transformatose, R.drawable.zaldior
    };

    private Integer [] enutrofIds = {};

    private Integer [] iopIds = {};

    private Integer [] sacrierIds = {};

    private Integer [] sadidaIds = {};

    private Integer [] sramIds = {};

    private Integer [] xelorIds = {};
}
