package com.lkledu.root.deckbuild;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.GridView;

/**
 * Created by root on 05/08/17.
 */

public class CardsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.cards_fragment,container,false);
        String godName = getActivity().getIntent().getExtras().getString("godName");
        final CardAdapter adapter = new CardAdapter(view.getContext(), godName);

        GridView cards = view.findViewById(R.id.cards_layout);
        cards.setAdapter(adapter);

        return (view);
    }
}


