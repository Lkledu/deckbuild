package com.lkledu.root.deckbuild;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;


/**
 * Created by root on 04/08/17.
 * Classe fragment do Deck
 */

public class DeckFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.deck_fragment, container, false);

        GridView deck = view.findViewById(R.id.deck_layout);
        return (view);
    }

}
